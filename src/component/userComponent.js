import React, { useEffect } from "react";
import { connect } from "react-redux";
import { fetchUsers } from "../redux";

const UserComponent = ({ userData, fetchUsers }) => {
  useEffect(() => {
    fetchUsers();
  }, []);
  console.log(userData.user.data);
  // return <h1>Hello</h1>;
  return userData.loading ? (
    <h2>Loading</h2>
  ) : userData.user.err ? (
    <h2>{userData.user.err}</h2>
  ) : (
    userData.user.data.map(item => <h2 key={item.id}>{item.name}</h2>)
  );
};

const mapStateToProps = state => {
  return {
    userData: state
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchUsers: () => dispatch(fetchUsers())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(UserComponent);
