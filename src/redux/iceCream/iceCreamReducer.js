import { BUY_ICECREAM } from "./iceCreamTypes";

const initIceCreamState = {
  numOfIceCreams: 20
};

export const iceCreamReducer = (state = initIceCreamState, action) => {
  switch (action.type) {
    case BUY_ICECREAM:
      return {
        ...state,
        numOfIceCreams: state.numOfIceCreams - 1
      };

    default:
      return state;
  }
};
