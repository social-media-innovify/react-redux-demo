import axios from "axios";
import {
  FETCH_USERS_REQUEST,
  FETCH_USERS_SUCCESS,
  FETCH_USERS_FAILURE
} from "./userTypes";

export const fetchUserRequest = () => {
  return {
    type: FETCH_USERS_REQUEST
  };
};

const fetchUserSuccess = users => {
  return {
    type: FETCH_USERS_SUCCESS,
    payload: users
  };
};

const fetchUserFailure = err => {
  return {
    type: FETCH_USERS_FAILURE,
    payload: err
  };
};

export const fetchUsers = () => {
  return dispatch => {
    axios
      .get("https://jsonplaceholder.typicode.com/users")
      .then(res => {
        const users = res.data;
        dispatch(fetchUserSuccess(users));
      })
      .catch(err => {
        const errorMsg = err.message;
        dispatch(fetchUserFailure(errorMsg));
      });
  };
};
