import {
  FETCH_USERS_REQUEST,
  FETCH_USERS_SUCCESS,
  FETCH_USERS_FAILURE
} from "./userTypes";

const initUserState = {
  loading: false,
  data: [],
  err: ""
};

export const userReducer = (state = initUserState, action) => {
  switch (action.type) {
    case FETCH_USERS_REQUEST:
      return {
        ...state,
        loading: true
      };
    case FETCH_USERS_SUCCESS:
      return {
        loading: false,
        data: action.payload,
        err: ""
      };
    case FETCH_USERS_FAILURE:
      return {
        loading: false,
        data: [],
        err: action.payload
      };

    default:
      return state;
  }
};
