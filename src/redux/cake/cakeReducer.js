import { BUY_CAKE } from "./cakeTypes";

const initCakeState = {
  numOfCakes: 10
};

export const cakeReducer = (state = initCakeState, action) => {
  switch (action.type) {
    case BUY_CAKE:
      return {
        ...state,
        numOfCakes: state.numOfCakes - action.payload
      };
    default:
      return state;
  }
};
