import React from "react";
import store from "./redux/store";
import { Provider } from "react-redux";
import "./App.css";
import CakeContainer from "./component/cakeContainer";
import HookCakeContainer from "./component/hookCakeContainer";
import IceCreamContainer from "./component/iceCreamContainer";
import NewCakeContainer from "./component/newCakeContainer";
import ItemContainer from "./component/itemContainer";
import UserComponent from "./component/userComponent";

function App() {
  return (
    <Provider store={store}>
      <div className="App">
        {/* <ItemContainer cake />
        <ItemContainer />
        <HookCakeContainer />
        <CakeContainer />
        <IceCreamContainer />
        <NewCakeContainer /> */}
        <UserComponent />
      </div>
    </Provider>
  );
}

export default App;
