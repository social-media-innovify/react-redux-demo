#REACT-REDUX-NOTES

==> Connect react and redux:-

    - create compActions.js, compTypes.js, compReducer.js of component in ./Redux/ComponentName folder

    - npm install redux react-redux

    - create store.js in ./Redux folder, and define createStore() method.
    (import {createStore} from 'redux')
    import rseducer that we created in compReducer.js
    pass compReducer in createStore(compReducer) method.

    - Export all necessity values from all files.

    - Create index.js in ./Redux folder, export every actions that we created from this file.

    - in App.js, import {Provider} from 'react-redux' and import store that we've created.
    Then add <Provider store={store}><Component /></Provider> as shown.

    - In Component/newComponent.js file import {action} form '../redux', it will export {action} from index.js that we've created above.

    - Import {connect} from 'react-redux' in newComponent.js

    - Add mapStateToProps, pass state as param.

    - Add mapDispatchToProps, pass dispatch as param.

    - At export line write connect(mapStateToProps, mapDispatchToProps)(NewComponent)
    In which 1st argument will be mapStateToProps and 2nd will be mapDispatchToProps

    - It will send data as props.

==> useSelector, useDispatch hook (Redux):-

    - import {useSelector, useDispatch} from 'react-redux'

    - useSelector is use for accessing state object of redux
    eg. const numOfCakes = useSelector(state=>state.numOfCakes)

    - useDispatch is use for dispatching action of redux.
    eg. const dispatch = useDispatch()
        <button onClick={()=>dispatch(buyCake())}>

    - In dispatch, CALL the function. not just pass as parameter.

==> Multiple Reducer:-

    - Same as learned in vanilla JS.

    - But create rootReducer.js file in ./Redux folder.

    - combine all reducers in that file and export default the rootReducer.

    - import that rootReducer in store.js.

==> redux dev tool

    - Install chrome extension redux devtool.

    - npm install --save redux-devtools-extension

    - import { composeWithDevTools } from "redux-devtools-extension";
    in store.js file

    - const store = createStore( rootReducer, composeWithDevTools( applyMiddleware(logger) ));
    Pass composeWithDevTools as 2nd argument, and in it, pass applyMiddleware.

    - You can inspect your state and other processes in chrome developer tool.

==> Action payload:-

    - Buy multiple cake at same time (pass number in input box and with help of useState)

    - also pass number variable to buyCake() function and do necessity changes.

    - Add payload object in cakeActions.js, and assign value of number to it.

    - In cakeReducer.js, substract action.payload from numOfCakes.

==> mapStateToProps:-

    - first parameter is state and 2nd parameter is ownProps.

    - We can access the props sended by parent to ownProps parameter.
    eg. <Component name="Avkash">
    const mapStateToProps=(state, ownProps)=>{
        console.log(ownProps.name);
    }

==> mapDispatchToProps:-

    - Same as mapStateToProps. It also get ownProps as second parameter.

==> Asynnc action in react redux (axios and thunk):-

    // Never forget to pass "default: return state;" in switch case of componentReducer.js file.
    // It will popup an error.
    It works same as we used in vanilla JS.
    eg. UserComponent and all user files.
